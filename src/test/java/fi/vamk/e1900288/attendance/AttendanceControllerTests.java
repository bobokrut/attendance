package fi.vamk.e1900288.attendance;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.apache.commons.collections4.IterableUtils;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.junit4.SpringRunner;

import java.sql.Date;
import java.time.LocalDate;


@RunWith(SpringRunner.class)
@SpringBootTest
@Configuration
@ComponentScan(basePackages = {"fi.vamk.e1900288.attendance"})
@EnableJpaRepositories(basePackageClasses = AttendanceRepository.class)
public class AttendanceControllerTests {

    @Autowired
    private AttendanceRepository repository;

    @Test
    public void postGetDeleteAttendance() {

        Iterable<Attendance> begin = repository.findAll();
        System.out.println(IterableUtils.size(begin));

        Attendance att = new Attendance("ABCD", Date.valueOf(LocalDate.now()));
        System.out.println("ATT: " + att.toString());
        Attendance saved = repository.save(att);
        System.out.println("SAVED: " + saved.toString());

        Attendance found = repository.findByKey(att.getKey());
        System.out.println("FOUND: " + saved.toString());
        assertEquals(att.getKey(), found.getKey());
        ;
        repository.delete(found);
        Iterable<Attendance> end = repository.findAll();
        // System.out.println(IterableUtils.size(end));
        assertEquals((long) IterableUtils.size(begin), (long) IterableUtils.size(end));
    }

    @Test
    public void postGetAttendanceWithDate() {

        Attendance attendance = new Attendance();

        attendance.setKey("TEST");
        attendance.setDate(Date.valueOf(LocalDate.parse("2022-01-01")));
        repository.save(attendance);

        Attendance attendance1 = repository.findByDate(Date.valueOf(LocalDate.parse("2022-01-01")));
        assertEquals(attendance.toString(), attendance1.toString());



    }
}
