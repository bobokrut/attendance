package fi.vamk.e1900288.attendance;


import java.io.Serializable;
import java.sql.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQuery;
import javax.validation.constraints.NotNull;

@Entity
@NamedQuery(name = "Attendanve.findAll", query = "select p from Attendance p")
public class Attendance implements Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;
    private String key;
    @NotNull(message = "Date is required")
    private Date date;

    public Attendance(int id, String key, Date date) {
        this.id = id;
        this.key = key;
        this.date = date;
    }

    public Attendance(String key) {
        this.key = key;
    }

    public Attendance() {

    }

    public Attendance(String key, java.sql.Date date) {
        this.key = key;
        this.date = date;
    }

    public int getId() {

        return this.id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getKey() {
        return this.key;
    }

    public void setKey(String key) {
        this.key = key;
    }

    public String toString() {
        return date + ": " + key;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

}
