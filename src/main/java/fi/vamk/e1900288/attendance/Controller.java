package fi.vamk.e1900288.attendance;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class Controller {
	@RequestMapping("/test")
	public String test() {
		return "{\"id\": 1}";
	}

	@GetMapping("/")
	public String swagger() {
		return "<script>window.location.replace(\"/swagger-ui.html\")</script>";
	}
}
