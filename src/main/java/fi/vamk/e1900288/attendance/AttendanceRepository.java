package fi.vamk.e1900288.attendance;

import org.springframework.data.repository.CrudRepository;

import java.util.Date;

public interface AttendanceRepository extends CrudRepository<Attendance, Integer> {

    public Attendance findByKey(String key);
    public Attendance findByDate(Date date);
}
